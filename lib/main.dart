import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:to_do_list/todo_model.dart';
import 'package:hive_flutter/hive_flutter.dart';

Future<void> main() async {
  // WidgetsFlutterBinding.ensureInitialized();
  // Directory document = await getApplicationDocumentsDirectory();
  // Hive.init(document.path);
  await Hive.initFlutter();
  Hive.registerAdapter(TodoModelAdapter());
  Hive.openBox<TodoModel>("todo");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Flutter Demo",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

enum TodoFilter {ALL, COMPLETED, INCOMPLETED}

class _MyHomePageState extends State<MyHomePage> {

  Box<TodoModel> todoBox = Hive.box<TodoModel>("todo");

  TextEditingController titleController = TextEditingController();
  TextEditingController detailController = TextEditingController();

  TodoFilter filter = TodoFilter.ALL;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text("To do list"),
        ),
        actions: [
          PopupMenuButton<String>(
            onSelected: (value){
              if(value.compareTo("All") == 0){
                setState((){
                  filter = TodoFilter.ALL;
                });
              }
              else if(value.compareTo("Completed") == 0){
                setState(() {
                  filter = TodoFilter.COMPLETED;
                });
              }
              else{
                setState(() {
                  filter = TodoFilter.INCOMPLETED;
                });
              }

            },

              itemBuilder: (BuildContext context){
                return ["All", "Completed", "Incompleted"].map((option) {
                  return PopupMenuItem(
                    value: option,
                    child: Text(option),
                  );
                }).toList();
              }
          )
        ],
      ),
      body: Column(
        children: [
          ValueListenableBuilder(
            valueListenable: todoBox.listenable(),
            builder: (BuildContext context, Box<TodoModel> todos, Widget? child) {

            List<int>? keys;

            if(filter == TodoFilter.ALL){
              keys = todos.keys.cast<int>().toList();
            }
            else if(filter == TodoFilter.COMPLETED){
              keys = todos.keys.cast<int>().where((key) => todos.get(key)!.isCompleted).toList();
            }
            else {
              keys = todos.keys.cast<int>().where((key) => !todos.get(key)!.isCompleted).toList();
            }

              return ListView.separated(
                itemBuilder: (context, index) {
                  final key = keys![index];
                  final TodoModel? value = todos.get(key);

                  return ListTile(
                    title: Text(value!.title, style: TextStyle(fontSize: 20),),
                    subtitle: Text(value.detail, style: TextStyle(fontSize: 16)),
                    leading: Text("$key"),
                    trailing: Icon(Icons.check, color: value.isCompleted ? Colors.green : Colors.red,),
                    onTap: (){
                      showDialog(
                          context: context,
                          builder: (BuildContext context){
                            return AlertDialog(
                              actions: [
                                Center(
                                  child: MaterialButton(
                                    onPressed: () {
                                      TodoModel todo = TodoModel(
                                          title: value.title,
                                          detail: value.detail,
                                          isCompleted: true
                                      );

                                      todoBox.put(key, todo);

                                      Navigator.pop(context);
                                    },
                                    child: const Text(
                                      "Mark as completed",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    color: Colors.blue,
                                  ),
                                ),
                              ],
                            );
                          }
                      );
                    },
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                const Divider(
                  indent: 20,
                  endIndent: 20,
                ),
                itemCount: keys.toList().length,
                shrinkWrap: true,
              );
            },
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          showDialog(
              context: context,
              builder: (BuildContext context){
                return AlertDialog(
                  title: Text("Add item"),

                  content: Container(
                      height: 180,
                      padding: EdgeInsets.all(32),
                      child: Column(
                        children: [
                          TextField(
                            decoration: InputDecoration(hintText: "Title"),
                            controller: titleController,
                          ),

                          SizedBox(height: 16,),

                          TextField(
                            decoration: InputDecoration(hintText: "Detail"),
                            controller: detailController
                          ),

                        ],
                      )
                  ),
                  actions: [
                    MaterialButton(
                      onPressed: () {
                        final String title = titleController.text;
                        final String detail = detailController.text;

                        TodoModel todo = TodoModel(title: title, detail: detail, isCompleted: false);

                        todoBox.add(todo);

                        Navigator.pop(context);
                      },
                      child: const Text(
                        "Submit",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                    ),
                  ],
                );
              }
          );

        },
      ),
    );
  }
}

